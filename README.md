# TumbledEmerald Scripts

## Description
This repository holds scripts and assets for the TumbledEmerald project.

[<img src="https://img.shields.io/badge/Gitpod-Open%20workspace-orange.svg?logo=GITPOD&style=for-the-badge">](https://gitpod.io/#gitlab.com/tbld/scripts.git)


## Usage
These scripts require a Linux-based system to run. Cloning the repo and running `main.sh` will print a handy help section for you.


## Contributing
Feel free to contribute to this repository! We need:
* Windows buildscripts
* Scripts for other distributions (NixOS, Fedora, etc.)

## Authors and acknowledgment
Scripts: Supersonic

## License
All scripts are licensed under MIT-0. Everything in `assets/agb_rfu` is considered proprietary.
