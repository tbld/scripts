# Copyright (c) 2022 tumbledemerald contributors
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights 
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
# copies of the Software, and to permit persons to whom the Software is furnished
# to do so.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
# PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
# CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
# THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#!/bin/sh
rm ../game/data/scripts/cable_club.inc
echo "Removed old Cable Club script."
rm ../game/data/scripts/pkmn_center_nurse.inc
echo "Removed old nurse script."
cp ./assets/agb_rfu/cable_club.inc ../game/data/scripts/cable_club.inc
echo "Copied patched Cable Club script."
cp ./assets/agb_rfu/pkmn_center_nurse.inc ../game/data/scripts/pkmn_center_nurse.inc
echo "Copied patched nurse script."
