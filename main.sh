# Copyright (c) 2022 tumbledemerald contributors
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights 
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
# copies of the Software, and to permit persons to whom the Software is furnished
# to do so.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
# PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
# CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
# THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#!/bin/sh
if [[ \ $*\  == *\ --debian\ * ]] || [[ \ $*\  == *\ -d\ * ]]
then
    echo "Compiling release on debian..."
    sh ./debian.sh
elif [[ \ $*\  == *\ --debian-debug\ * ]] || [[ \ $*\  == *\ -D\ * ]]
then
    echo "Compiling debug on debian..."
    sh ./debian-debug.sh
elif [[ \ $*\  == *\ --ubuntu\ * ]] || [[ \ $*\  == *\ -u\ * ]]
then
    echo "Compiling release on ubuntu..."
    sh ./debian.sh
elif [[ \ $*\  == *\ --ubuntu-debug\ * ]] || [[ \ $*\  == *\ -U\ * ]]
then
    echo "Compiling debug on ubuntu..."
    sh ./debian-debug.sh
elif [[ \ $*\  == *\ --porymap-ubuntu\ * ]] || [[ \ $*\  == *\ -p\ * ]]
then
    echo "Compiling porymap on ubuntu..."
    sh ./ubuntu-porymap.sh
elif [[ \ $*\  == *\ --archlinux\ * ]] || [[ \ $*\  == *\ -a\ * ]]
then
    echo "Compiling release on archlinux..."
    sh ./archlinux.sh
elif [[ \ $*\  == *\ --archlinux-debug\ * ]] || [[ \ $*\  == *\ -A\ * ]]
then
    echo "Compiling debug on archlinux..."
    sh ./archlinux-debug.sh
elif [[ \ $*\  == *\ --archlinux-porymap\ * ]] || [[ \ $*\  == *\ -P\ * ]]
then
    echo "Compiling porymap on archlinux..."
    sh ./archlinux-porymap.sh
elif [[ \ $*\  == *\ --gitpod\ * ]] || [[ \ $*\  == *\ -g\ * ]]
then
    echo "Compiling release on gitpod..."
    sh ./gitpod.sh
elif [[ \ $*\  == *\ --gitpod-debug\ * ]] || [[ \ $*\  == *\ -G\ * ]]
then
    echo "Compiling debug on gitpod..."
    sh ./gitpod-debug.sh
elif [[ \ $*\  == *\ --gitpod-debug\ * ]] || [[ \ $*\  == *\ -G\ * ]]
then
    echo "Patching wireless functionality..."
    sh ./clone.sh
    sh ./enable_rfu.sh
elif [[ \ $*\  == *\ --help\ * ]] || [[ \ $*\  == *\ -h\ * ]]
then
    sh ./help.sh
else
    sh ./help.sh
fi
