#!/bin/sh
# Copyright (c) 2022 tumbledemerald contributors
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights 
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
# copies of the Software, and to permit persons to whom the Software is furnished
# to do so.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
# PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
# CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
# THE USE OR OTHER DEALINGS IN THE SOFTWARE.


printf "TECS\nThe TumbledEmerald Automatic Compiler\n"
printf "\n"
printf "'-h' or '--help'\nDisplays this help dialogue.\n"
printf "\n"
printf "'-d or '--debian'\nCompiles a release build of TumbledEmerald on a Debian GNU/Linux system."
printf "\n"
printf "'-D' or '--debian-debug'\nCompiles a debug build of TumbledEmerald on a Debian GNU/Linux system.\nPlease note that the debug menu is currently a work-in-progress."
printf "\n"
printf "'-u' or '--ubuntu'\nCompiles a release build of TumbledEmerald on an Ubuntu system."
printf "\n"
printf "'-U' or '--ubuntu-debug'\nCompiles a debug build of TumbledEmerald on an Ubuntu system.\nPlease note that the debug menu is currently a work-in-progress."
printf "'-p' or '--ubuntu-porymap'\nCompiles the map editor 'porymap' on an Ubuntu system."
printf "\n"
printf "'-a' or '--archlinux'\nCompiles a release build of TumbledEmerald on an Arch Linux system."
printf "\n"
printf "'-A' or '--archlinux-debug'\nCompiles a debug build of TumbledEmerald on an Arch Linux system\nPlease note that the debug menu is currently a work-in-progress."
printf "\n"
printf "'-P' or '--archlinux-porymap'\nCompiles the map editor 'porymap' on an Arch Linux system."
printf "\n"
printf "'-g' or '--gitpod'\nCompiles a release build of TumbledEmerald in the Gitpod web terminal."
printf "\n"
printf "'-G' or '--gitpod-debug'\nCompiles a debug build of TumbledEmerald in the Gitpod web terminal.\nPlease note that the debug menu is currently a work-in-progress."
printf "\n"
printf "'-w' or '--enable-wireless-adapter'\nPatches the source tree to re-enable Wireless Adapter functionality for flashcart users.\nThis will also clone the game code for you."
