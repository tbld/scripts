# Copyright (c) 2022 tumbledemerald contributors
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights 
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
# copies of the Software, and to permit persons to whom the Software is furnished
# to do so.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
# PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
# CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
# THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# This shell script is built for the Gitpod web terminal. 
# If you're using a standard Linux distribution/WSL install,
# this is not the script for you.

# This script builds a debugging copy of tumbledemerald. 
# If you just want to play the game, don't use this script.

#!/bin/sh
mkdir temp
cd temp
xargs -a "./assets/deps/gitpod-game-dependencies.txt" sudo apt install
sh ./clone.sh
cd game
cd agbcc
./build.sh && ./install.sh ../
cd ..
make DDEBUG=1 -j16
